# sr2i204-totp

Project related to the SR2I204 course. It encompasses the implementation of the TOTP Algorithm for a website. 

## Setup

### Getting started
Clone the [project repository](https://gitlab.com/TheRandom/sr2i204-totp.git) using the following command :
`$ git clone https://gitlab.com/TheRandom/sr2i204-totp.git`

### Installation
Run the following command to install required modules
`$ pip install -r requirements.txt`

### Usage
Run the following command to launch the project
`$ python3 app.py`


Testing credentials : 
- email : `test@mail.fr`
- password : `password`

## Roadmap
- Create the website
- Add connexion form
- Add database query to authenticate user
- Add TOTP authentication
- Add form to create account
- Deploy the website


## Test and Deploy
This section will contains every test and deployment tasks that we run on our project.


## TODO

### Feature-wise
- [x] Add js to check session cookie (is connected ?)
- [x] Add API /verify to verify cookie
- [x] Add TOTP submit when authenticating
- [x] Add logout button (delete cookie in db and delete user cookie)
- [x] Add js function message to show user if connected or not
- [x] Add account creation form
- [ ] Add TOTP activation (form) when connected
- [ ] Make our own module for totp pin generation
- [ ] Improve website design (navbar, css for js paragraph creation ...)

### Security-wise
- [ ] Fix SQL injection on forms

### Optional fixes
- [ ] Make condition on auth api so it is the only path when puting credentials and totp
- [ ] Add unit test python


## Authors and acknowledgment
Authors : Isaïe, Théophile.


## License

web_totp, a basic totp-login implementation for websites\
Copyright (C) 2022  TheRandom, imrn

This program is free software: you can redistribute it and/or modify 
it under the terms of the GNU Affero General Public License as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU Affero General Public License for more details.

You should have received a copy of the **GNU Affero General Public License** 
along with this program.  If not, see <https://www.gnu.org/licenses/>.


## Project status
WIP


## Bibliography
- TOTP explanation and summary: https://www.ionos.com/digitalguide/server/security/totp/
- IETF documentation concerning TOTP: https://datatracker.ietf.org/doc/html/rfc6238
- IETF documentation concerning HOTP: https://datatracker.ietf.org/doc/html/rfc4226
- IETF documentation concerning HMAC: https://datatracker.ietf.org/doc/html/rfc2104
- Wikipedia TOTP webpage: https://en.wikipedia.org/wiki/Time-based_One-Time_Password
- Wikipedia HMAC functions webpage : https://en.wikipedia.org/wiki/HMAC
- Reference for the website creation: https://www.section.io/engineering-education/implementing-totp-2fa-using-flask/
- Repository of the OTP module used, PyOTP: https://github.com/pyauth/pyotp
- Documentation of the OTP module used, PyOTP: https://pyauth.github.io/pyotp/
