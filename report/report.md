---
title: 'SR2I-204 Project Report'
author: Théophile Reverdell, Isaïe Muron
...


\newpage
## Preamble

### License 

web_totp is a basic totp-login implementation for websites\
Copyright (C) 2022  TheRandom, imrn

**Concerning the associated code**:\
This program is free software: you can redistribute it and/or modify 
it under the terms of the GNU Affero General Public License as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU Affero General Public License for more details.

You should have received a copy of the **GNU Affero General Public License** 
along with this program.  If not, see <https://www.gnu.org/licenses/>.


### Getting started

This project consists in the creation of a website implementing the TOTP algorithm for authentication.


Clone the [project repository](https://gitlab.com/TheRandom/sr2i204-totp.git) using the following command :\
`$ git clone https://gitlab.com/TheRandom/sr2i204-totp.git`


**Information about requirements, setting-up and running the code is provided in the README.md file**. This report only encompasses the usage, observations and technicalities made throughout the project. The references used can be found in both this report and the README.md file.


\newpage
## Architecture

### Usage scenario

```
    +-------------------+
    |                   |
    |                   |
    |      DATABASE     |
    |                   |
    |                   |
    +------^-----^------+
           |     |
           |     |
           2     4
           |     |
           |     |
    +------|-----|------+
    |                   |       1. The user send its credentials 
    |                   |       
    |      WEBSITE      |
    |                   |       2. The server verify the credentials in the database. 
    |                   |
    +---^-----^-----|---+       3. The user is indentified (receive cookie) then sends its TOTP. 
        |     |     |            
        |     |     |
        1     3     5           4.  The server verify TOTP.
        |     |     |           
        |     |     |            
    +---|-----|-----v---+
    |                   |       5.  The user is authenticated and receive cookie.
    |                   |       
    |        USER       |
    |                   |       
    |                   |       
    +-------------------+
```


### Implementation

The project is composed of 3 elements interacting with each others.


**Firstly we have our database that contains every accounts**. It consists of one table (`totp_accounts`) with 6 columns :

- `id int` : id of the account
- `email text` : email address of the user
- `password text` : password of the user
- `session_id text` : cookie with session id which can identify the user
- `auth_cookie text` : authentication cookie (to verify authentication)
- `totp text` : totp secret key
- `totp_enable bool` : boolean to enable totp authentication method or not


**Then we have our API (backend) that is the link between our web app and the database**. The API receive data (credentials for example) through a POST request and store it in database. The API got 4 path :

- the root path (`/`) which consist of the homepage with totp master key information
- the authentication path (`/auth`) which consist of credentials receiver and verification with the database
- the totp authentication path (`/totp_auth`) which consist of Temporary One Time Password receiver.
- the verification path (`/verif`) that allow verification of user authentication and that returns totp secret key.


**Finally we have our user interface** that consists of a minimal web interface made with html, native js and bootstrap. 
The web interface consist of 3 pages :

- the home page (`/`) which contains logout button and TOTP secret key.
- the authentication page (`/auth`) that contains a form with email and password input
- the totp authentication page (`/totp_auth`) that contains a form with temporary password input


\newpage
## Usage

**Run the following command to launch the project** :\
`$ python3 app.py`

**Testing credentials** : 

- email : `test@mail.fr`
- password : `password`

### Authentication without TOTP

The user need to go to `/auth` page and sumbit his credentials.

![Authentication page](./img/auth_page.png){width=80%}\

Then the backend will get his credentials compare it with the database. When credentials are validated, the server will send to the user a session id cookie to identify him and an authentication cookie that authenticate the session.

![Server log for normal authentication](./img/cookie.png){width=100%}\

User is then redirected to the main page where he can see his TOTP secret key.

![Main page](./img/main_page_auth.png){width=35%}\

The server receives two requests for this authentication :

- The first request is a GET `/auth` to get authentication form
- The second request is a POST request to `/auth` to submit credentials

![Server log for normal authentication](./img/log_auth.png){width=100%}\

### Authentication with TOTP 

User submit his credentials on `/auth` page as previously mentionned. The server will give him a session id cookie which identify the user and will redirect him to `/totp_auth` page. This page is a form with password input for Temporary One Time Password submission.

![Main page](./img/totp_page.png){width=80%}\

The user submit his temporary password and the server verify it and then send to him an auth cookie to validate his authentication.

The server receives 4 requests for this authentication :

- The first request is a GET `/auth` to get authentication form
- The second request is a POST request to `/auth` to submit credentials
- The third request is a GET request to `/totp_auth` page to get totp submission form
- The last request is a POST request to `/totp_auth` page to submit temporary password

![Server log for TOTP authentication](./img/log_totp.png){width=100%}\


\newpage
## Algorithmics behind TOTP

The numbers presented to the user as the _time-based one-time password_ is the truncated result of a HMAC function. The exact function may vary from 
one implementation to another. We decided to write one such implementation for learning and illustrative purposes, while using a more complete, already 
existing module in our website. Our implementation of a TOTP generating function can be found in the `crypto.py` file. It contains an implementation from 
scratch of the SHA-256 and HMAC-SHA-256 functions.

HMAC functions are based on specific hashing functions and take as arguments a secret key, as well as a message. The following is the IETF definition (RFC 2104): 

![HMAC definition](./img/hmac.png){width=60%}\

- `H` is a hashing function. Our implementation will use the SHA-256 from the SHA2 family.
- `m` is the message input. For the TOTP algorithm, m has a specific value.
- `K` is the secret key. `K'` is an alternative variable being either equal to `K` or `H(K)`. The block size for SHA-256 being 512 bits, we would have `K=K'`.
- `ipad` and `opad` are cosntants which repspective lengths depend on block size.

TOTP is a variant on the HOTP algorithm, both being specific use-case of HMAC functions. In the HOTP algorithm, the message `m` is replaced by a counter shared 
between user and server. Note that this counter does not have to be secret. In the case of TOTP algorithm, this counter is replaced by a time-based specific counter. 
According to the IETF (RFC 6238), this counter `T` represents the number of time steps taken since a referenced starting time. The time-step and initial time are 
implementation-dependant, although a common setup is a 30 seconds time-step `X`, and a 0 starting time `T0`. You then have `T = floor((T-T0)/X)`.

The security of this password-generating algorithm is entirely dependant on the security of the hashing function used. However, because of the format itself of the 
resulting one-time password, it is usually acceptable


\newpage
## Future additions

### Feature-wise 

- Add TOTP activation (form) when connected
- Make our own module for totp pin generation
- Improve website design (navbar, css for js paragraph creation)

### Security-wise

- Fix SQL injection on forms

### Optionnal 

- Make condition on auth api so it is the only path when puting credentials and totp
- Add unit test python

### Others

- Make a more complete documentation for the usage of the source code


## Bibliography, references
- TOTP explanation and summary: https://www.ionos.com/digitalguide/server/security/totp/
- IETF documentation concerning TOTP: https://datatracker.ietf.org/doc/html/rfc6238
- IETF documentation concerning HOTP: https://datatracker.ietf.org/doc/html/rfc4226
- IETF documentation concerning HMAC: https://datatracker.ietf.org/doc/html/rfc2104
- Wikipedia TOTP webpage: https://en.wikipedia.org/wiki/Time-based_One-Time_Password
- Wikipedia HMAC functions webpage : https://en.wikipedia.org/wiki/HMAC
- Reference for the website creation: https://www.section.io/engineering-education/implementing-totp-2fa-using-flask/
- Repository of the OTP module used, PyOTP: https://github.com/pyauth/pyotp
- Documentation of the OTP module used, PyOTP: https://pyauth.github.io/pyotp/



