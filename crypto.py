# web_totp, a basic totp-login implementation for websites
# Copyright (C) 2022  TheRandom, imrn
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


####
#### According to IETF documentation, TOTP is defined as H(K,nT) with nT the number of
#### time steps taken since the initial counter time and K the secret key. H may be
#### HMAC-SHA-256 or HMAC-SHA-512. For our implementation we will use HMAC-SHA-256,
#### that is defined as: (|| represents a concatenation, m the input, opad/ipad constants)
#### sha256((K xor opad) || sha256((K xor ipad) || m ))
####
#### The intermediate function were written and named using Wikipedia as a reference:
#### https://en.wikipedia.org/wiki/SHA-2
####

from time import time
from base64 import b64decode


def utf8_to_binary(st):
    formatted_binary = ''.join(map(bin, bytearray(st, 'utf8')))
    split_binary = formatted_binary.split('0b')
    split_binary.pop(0)

    # add padding to write each character on 8 bits
    result = ''
    for obj in split_binary:
        for i in range(0, 8 - len(obj)):
            obj = '0' + obj
        result += obj
    return result


def pad_to_correct_length(st):
    bin_length = len(st)
    k = (448 - bin_length - 1) % 512
    # next line : k-7>=0 because each character is coded on 8 bits
    padding_bits = '10000000' + (k - 7) * '0' + format(bin_length, '#066b')[2::]
    return st + padding_bits


# Functions used during bit manipulation


def rotation_right(n, x):
    return (x >> n) | (x << (32 - n))


def shift_r(n, x):
    return x >> n


def sigma_maj_0(x):
    return rotation_right(2, x) ^ rotation_right(13, x) ^ rotation_right(22, x)


def sigma_maj_1(x):
    return rotation_right(6, x) ^ rotation_right(11, x) ^ rotation_right(25, x)


def sigma_min_0(x):
    return rotation_right(7, x) ^ rotation_right(18, x) ^ shift_r(3, x)


def sigma_min_1(x):
    return rotation_right(17, x) ^ rotation_right(19, x) ^ shift_r(10, x)


def ch(x, y, z):
    return (x & y) ^ (~x & z)


def maj(x, y, z):
    return (x & y) ^ (x & z) ^ (y & z)


# Main functions


def sha256(input_str):
    # initialize const list
    h_list = [0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19]
    k_256_list = [0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
                  0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
                  0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
                  0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
                  0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
                  0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
                  0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
                  0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2]

    # input
    binary_str = utf8_to_binary(input_str)
    padded_str = pad_to_correct_length(binary_str)
    n = int(len(padded_str) / 512)  # block amount

    # hashing
    for i in range(1, n + 1):
        current_block = padded_str[(i - 1) * 512:i * 512]
        w = []
        a = h_list[0]
        b = h_list[1]
        c = h_list[2]
        d = h_list[3]
        e = h_list[4]
        f = h_list[5]
        g = h_list[6]
        h = h_list[7]
        for t in range(64):
            if t < 16:
                w.append(int(current_block[t * 32:(t + 1) * 32], 2))
            else:
                w.append((sigma_min_1(w[t - 2])
                          + w[t - 7]
                          + sigma_min_0(w[t - 15])
                          + w[t - 16]) % (2 ** 32))
            t1 = (h + sigma_maj_1(e) + ch(e, f, g) + k_256_list[t] + w[t])
            t2 = (sigma_maj_0(a) + maj(a, b, c))
            h = g
            g = f
            f = e
            e = (d + t1) % (2 ** 32)
            d = c
            c = b
            b = a
            a = (t1 + t2) % (2 ** 32)
        h_list[0] = (a + h_list[0]) % (2 ** 32)
        h_list[1] = (b + h_list[1]) % (2 ** 32)
        h_list[2] = (c + h_list[2]) % (2 ** 32)
        h_list[3] = (d + h_list[3]) % (2 ** 32)
        h_list[4] = (e + h_list[4]) % (2 ** 32)
        h_list[5] = (f + h_list[5]) % (2 ** 32)
        h_list[6] = (g + h_list[6]) % (2 ** 32)
        h_list[7] = (h + h_list[7]) % (2 ** 32)
    result = ''
    for obj in h_list:
        result += hex(obj)[2::]
    return result


def hmac_sha256(b64_key, t0, time_step):
    counter = int(time.time() - t0 / time_step)
    """str_key should be less than 512 bits long. If it is more than 512 bits long, use sha256(str_key) instead"""
    ipad = int("36"*64, 16)  # 64 repetitions of 36 in hex base, for a 512 bits sized variable
    opad = int("5c"*64, 16)  # same but with 5c
    key = base64.b64decode(b64_key)
    key = str(key, "utf-8")
    key = utf8_to_binary(key)
    return sha256(str(int(key, 2) ^ opad)
                  + str(sha256(str(int(key, 2) ^ ipad) + str(counter))))
