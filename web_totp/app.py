# web_totp, a basic totp-login implementation for websites
# Copyright (C) 2022  TheRandom, imrn
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from flask import Flask, Response, request, make_response, redirect, json, render_template
import sqlite3
import logging

# To generate random string
import random
import string

# For totp
from datetime import datetime
import pyotp
import base64
import qrcode
from PIL import Image
from io import BytesIO

# Global Vars
db = "totp.db"
logging.basicConfig(level=logging.DEBUG)

# APP creation
def create_app():
    app = Flask(__name__, template_folder='www')

    #### Web interface ####
    @app.route("/")
    def hello_world():
        logging.info("[!] Entering in hello world")
        try:
            auth = request.cookies.get('auth_cookie')
            session = request.cookies.get('session_id')
            logging.info("[!] session =", session, " auth =", auth)
            verif = db_verify(db, session, auth)
            logging.info("[!] verif=", verif)
            if verif:
                resp = make_response(render_template("index.auth.html"))
            else:
                resp = make_response(render_template("index.html"))
        except:
            logging.error("[x] Error no cookie are provided")
            resp = make_response(render_template("index.html"))
        finally:
            # Allow origin from everywhere to avoid error
            resp.headers['Access-Control-Allow-Origin'] = '*'
            return resp

    @app.route("/auth")
    def auth_page():
        # If user is already authenticated redirect to homepage
        try:
            auth_cookie = request.cookies.get('auth_cookie')
            session_cookie = request.cookies.get('session_id')
            verif = db_verify(db, session_cookie, auth_cookie)
            if verif:
                response = make_response(redirect('/'))
                return response
        except:
            logging.error("[x] Error while acquiring auth_cookie")

        resp = make_response(render_template("auth.html"))
        # Allow origin from everywhere to avoid error
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

    # TODO: Add condition to allow auth process on only /auth path
    @app.route("/totp_auth")
    def auth_totp():
        resp = make_response(render_template("totp_auth.html"))
        # Allow origin from everywhere to avoid error
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

    @app.route("/create_account")
    def create_account_page():
        resp = make_response(render_template("creation.html"))
        return resp

    #### API ####

    # Allow only POST request
    @app.route("/totp_auth", methods=['POST'])
    def api_auth_totp():
        date = datetime.now()
        # Get session cookie
        try:
            session = request.cookies.get('session_id')
        except:
            print("[x] Error no session cookie")
            # Redirect to auth page in case of error
            response = make_response(redirect("/auth"))
            return response
        # Get totp password
        password = request.form.get('inputTotp')
        verif = db_verify_totp(db, session, password, date)
        if verif:
            # Redirect to home page
            response = make_response(redirect('/'))
            # Set connection cookie
            response.set_cookie('auth_cookie', db_connect_user(db, session))
        else:
            print("[!] Error password mismatch")
            response = make_response(redirect('/totp_auth'))

        # Allow origin from everywhere to avoid error
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

    # Allow only POST request
    @app.route("/auth", methods=['POST'])
    def api_auth():
        # Getting information from form
        email = request.form.get('inputEmail')
        password = request.form.get('inputPassword')
        # Printing cred to cli
        print("[*] Password =", password, " email=",email)
        
        # Create session_id cookie
        cookie = db_identify_user(db, email, password) 
        totp_status = db_is_totp_enable(db, cookie)
        if totp_status:
            # Redirect to totp auth
            response = make_response(redirect('/totp_auth'))
            # Set connection cookie
            cookie = db_identify_user(db, email, password) 
            response.set_cookie('session_id', cookie) 
        else:
            # Otherwise return an auth cookie
            # Redirect to home page
            response = make_response(redirect('/'))
            # Set connection cookie
            response.set_cookie('session_id', cookie) 
            response.set_cookie('auth_cookie', db_connect_user(db, cookie))
        
        # Allow origin from everywhere to avoid error
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response

    # API to get totp password
    @app.route("/get_totp", methods=['POST']) 
    def get_totp():
        logging.info("[!] Getting totp secret")
        try:
            auth = request.cookies.get('auth_cookie')
            session = request.cookies.get('session_id')
            logging.info("[!] session=", session, " auth =", auth)
            totp = db_get_totp_secret(db, session, auth)
            buffer = BytesIO()
            img = qrcode.make(totp)
            img.save(buffer, format="JPEG")
            img = base64.b64encode(buffer.getvalue())
            img = img.decode("utf-8")
            logging.info("[!] Type img=", img)
            logging.info("[!] totp=", totp)
            totp = {'totp': totp, 'qr': img}
        except:
            loggin.error("[x] Error no cookie provided")
            totp = {'totp': 'None'}
            
        response = Response(json.dumps(totp))
        # Allow origin from everywhere to avoid error
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response


    # Verify api: verify user connection cookie
    @app.route("/verify", methods=['POST']) 
    def verify():
        try:
            auth_cookie = request.cookies.get('auth_cookie')
            session_cookie = request.cookies.get('session_id')
            logging.info("[!] Cookie = ", auth_cookie)
            verif = db_verify(db, session_cookie, auth_cookie)
            logging.info("[!] Verif:", verif)
            json_verify = {'verif': verif}
            # Create response object
            resp = Response(json.dumps(json_verify))
        except:
            logging.error("[x] Error no cookie provided")

        # Allow origin from everywhere to avoid error
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

    @app.route("/logout", methods=['POST'])
    def logout():
        try:
            auth = request.cookies.get('auth_cookie')
            session = request.cookies.get('session_id')
            logging.info("[!] Removing cookies from db")
            db_remove_cookie(db, session, auth)
        except:
            logging.error("[x] Error user auth info are wrong")

        response = make_response(redirect('/'))
        return response

    @app.route("/create_account", methods=['POST'])
    def create_account():
        try:
            email = request.form.get('inputEmail')
            password = request.form.get('inputPassword')
        except:
            logging.error("[x] Error account info are wrong")

        db_add_account(db, email, password)
        response = make_response(redirect("/auth"))
        return response

    return app

    
    

#### Random cookie ####
def random_cookie(length):
    r_string = random_str(length)
    cookie = base64.b64encode(bytes(r_string, encoding="utf-8"))
    return cookie

def random_str(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    #print("Random string of length", length, "is:", result_str)
    return result_str

#### Database ####
def db_connect(db):
    table = "totp_accounts"
    con, cur = db_get_cursor(db)
    return table, con, cur
def db_get_cursor(db):
    try:
        conn = sqlite3.connect('totp.db')
    except:
        logging.error("[x] Error when connecting to db")
        exit(1)
    return conn, conn.cursor()

def db_populate(db):
    # Create table
    table, con, cur = db_connect(db)
    cur.execute("""CREATE TABLE """ + table + """ (
            id int,
            user text,
            password text,
            session_id text,
            auth_cookie text,
            totp text,
            totp_enable bool
        );"""
    )
    # Save changes
    con.commit()


# Insert new account in db
def db_add_account(db, user, password):
    table, con, cur = db_connect(db)
    totp = random_str(12)
    logging.info("[+] TOTP secret char = ", totp)
    # SQL command
    cur.execute("""INSERT INTO """ + table + """ (id, user, password, totp, totp_enable)
            VALUES (
                1, '""" + 
                user + 
                """', '""" +
                password +
                """', '""" +
                totp +
                """', '""" +
                '0' +
            """' );"""
    )
    # Save changes
    con.commit()

# Insert totp test account
def db_add_totp_sample_account(db, user, password):
    # Create table
    table, con, cur = db_connect(db)
    # Add account with totp enable
    totp = random_str(12)
    cur.execute("""INSERT INTO """ + table + """ (id, user, password, totp, totp_enable)
            VALUES (
                1, '""" + 
                user +
                """', '""" +
                password +
                """', '""" +
                totp +
                """', '""" +
                '1' +
            """' );"""
    )
    # Save changes
    con.commit()

def db_verify(db, session_cookie, auth_cookie):
    if session_cookie:
        table, con, cur = db_connect(db)
        cur.execute("""SELECT auth_cookie FROM """ + table + """ WHERE session_id='""" + session_cookie + """';""")
        rows = cur.fetchall()
        result = rows[0][0]
        logging.info("[!] Type rows:", type(rows))
        logging.info("[!] Rows:", result)
        if result:
            return result == auth_cookie
    else:
        return False

def db_verify_totp(db, session, password, date):
    table, con, cur = db_connect(db)
    # Get totp secret from database
    cur.execute("""SELECT totp FROM """ + table + """ WHERE session_id='""" + session + """';""")
    rows = cur.fetchall()
    totp = rows[0][0]
    # Generate totp from now
    # TODO: Fix TOTP add to app
    logging.info("[!] TOTP base str : %s", totp)
    logging.info("[!] TOTP encoded in bytes: %s", bytes(totp, encoding='utf-8'))
    logging.info("[!] TOTP encoded in base64: %s", base64.b32encode(bytes(totp, encoding='utf-8')))
    totp = pyotp.TOTP(base64.b32encode(bytes(totp, encoding='utf-8')))
    logging.info("[!] TOTP object args : %s", totp.__dict__)
    totp = totp.at(date)
    logging.info("[!] hotp now = ", totp)
    # Return if match or not 
    return totp == password

# Connect user and stor connection cookie
def db_identify_user(db, user, password):
    table, con, cur = db_connect(db)
    cookie = random_str(12)
    cur.execute("""UPDATE """+ table + """ SET session_id = '""" +
            cookie +
            """' WHERE user='"""+
            user +
            """' AND password = '""" +
            password +
            """';"""
    )
    # Save changes
    con.commit()
    return cookie

def db_connect_user(db, session_cookie):
    table, con, cur = db_connect(db)
    auth_cookie = random_str(12)

    cur.execute("""UPDATE """+ table + """ SET auth_cookie = '""" +
            auth_cookie +
            """' WHERE session_id='"""+
            session_cookie +
            """';"""
    )
    # Save changes
    con.commit()
    return auth_cookie

# Check if totp is set up by the user
def db_is_totp_enable(db, cookie):
    table, con, cur = db_connect(db)

    # Check if TOTP is enable
    cur.execute(""" SELECT totp_enable FROM """ + table + """ WHERE session_id='""" + cookie + """'""")
    rows = cur.fetchall()
    totp_status = rows[0][0]
    logging.info("[!] Totp status", totp_status)
    return totp_status

# Set totp_enable of an account to 1
def db_set_totp_enable(db, cookie):
    table, con, cur = db_connect(db)

    # Set totp_enable to 0
    cur.execute("""UPDATE"""+ table 
        + """ SET totp_enable = '1' """ 
        + """ WHERE session_id='""" + cookie + """';""")
    # Save changes
    con.commit()

# Set totp_enable of an account to 0
def db_clear_totp_enable(db, cookie):
    table, con, cur = db_connect(db)

    # Set totp_enable to 0
    cur.execute("""UPDATE"""+ table 
        + """ SET totp_enable = '0' """ 
        + """ WHERE session_id='""" + cookie + """';""")
    # Save changes
    con.commit()

# Remove cookies from database
def db_remove_cookie(db, session, auth):
    table, con, cur = db_connect(db)
    print("[!] Removing cookie ", session, " and ", auth)
    cur.execute("""UPDATE """ + table 
        + """ SET auth_cookie=NULL"""
        + """ WHERE session_id='""" + session
        + """' AND auth_cookie='""" + auth + """';""")
    cur.execute("""UPDATE """ + table 
        + """ SET session_id=NULL"""
        + """ WHERE session_id='""" + session + """';""")
    
    # Save changes
    con.commit()

# Get totp secret when user is authenticated
def db_get_totp_secret(db, session, auth):
    table, con, cur = db_connect(db)
    logging.info("[!] Querying database for totp secret")
    cur.execute("""SELECT totp FROM """ + table
            + """ WHERE session_id='"""
            + session
            + """' AND auth_cookie='"""
            + auth
            + """';"""
    )
    rows = cur.fetchall()
    logging.info("[!] TOTP rows = ", rows)
    totp = rows[0][0]
    totp = base64.b32encode(bytes(totp, encoding='utf-8')).decode('utf-8')
    logging.info("[!] Totp secret = ", totp)
    return totp





def main():
    logging.info("[!] Creating object")
    db_populate(db)
    logging.info("[!] populating db")
    # Add sample accounts
    db_add_account(db, "test@mail.fr", "password")
    db_add_totp_sample_account(db, "totp@mail.fr", "password")
    # Create app
    app = create_app()
    # run app in debug mode on port 5000
    # Disabled debug mode because code is running twice
    app.run(debug=True, use_reloader=False, host='0.0.0.0', port=5000)

# If executed directly
if __name__ == '__main__':
    main()
