# web_totp, a basic totp-login implementation for websites
# Copyright (C) 2022  TheRandom, imrn
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import pytest

from web_totp import app

@pytest.fixture
def client():
    serv = app.create_app()
    with serv.test_client() as client:
        yield client

def test_login(client):
    rv = client.get("/auth")
    # Get every attributes of rv
    print("[*] Rv attributes =", rv.__dict__)
    f = open('web_totp/www/index.html', 'r')
    page = f.read()
    f.close()
    assert "200 OK" == rv._status
